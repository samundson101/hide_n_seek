#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT 28900
#define PILOT "10.115.20.250"
#define NAME "mbeall" 


struct sockaddr_in initializeAddress() {
	struct sockaddr_in server_addr;
	memset(&server_addr, '0', sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	return server_addr;
}

int initializeSocket() {
	int sockfd;
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if (sockfd == -1) {
		printf("Socket Error\n");
		return -1;
	}
	return sockfd;
}

int CConnect(char *IP) {
	int sockfd = initializeSocket();
	struct sockaddr_in server_addr = initializeAddress();

	int init = inet_pton(AF_INET, IP, &server_addr.sin_addr);
	if (init < 1) {
		printf("Error: inet_pton");
		return -1;
	}
	if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
		printf("Can't connect at %s\n", IP);
		return -1;
	}

	return sockfd;
}



int procedure(char *IP) {
	int sockfd;
	char buffer[256];
	char *victim, *ap;

	if ((sockfd = CConnect(IP)) < 1){ 
		return -1;
	}
	printf("Connected to %s\n", IP);

	if (send(sockfd, "Who are you?\n", 13, 0) < 1) {
		return -1;
	}
	if (recv(sockfd, buffer, 256, 0) < 1) {
		return -1;
	}


	printf("Found %s",buffer);

	if (buffer == "mbeall") {
		printf("Avoiding mbeall");
		close(sockfd);
		return -1;
	}
	if (buffer == "draustin") {
		printf("Avoiding draustin");
		close(sockfd);
		return -1;
	}
	if (buffer == "samundson") {
		printf("Avoiding samundson");
		close(sockfd);
		return -1;
	}
	if (buffer == "mamiller") {
		printf("Found mamiller! bonus points?");
		close(sockfd);
		return -1;
	}

	int pilotfd = CConnect(PILOT);
	close(pilotfd);
	close(sockfd);

	pthread_t thread_id;
}


int scan() { 
	char host[32];
	int i;
	int k;
	while (1) {
		for (k=1; k<=253; k++) {
			for (i=1; i<=253; i++) {
				sprintf(host, "10.90.%d.%d", i, k);
				sprintf(host, "10.124.%d.%d", i, k);
				procedure(host);
			}
		}
	}
}
